import argparse
import itertools

class Node():
    # Class representing a node in a tree.
    
    def __init__(self, name, parent=None):
        self.name = name
        self.parent = parent
        
    def __repr__(self):
        return self.name
    
    def get_ancestors(self):
        # Creates a generator that will yield ancestors
        # starting from the root of our tree. 
        if self.parent is None:
            return
        else:
            # yield from our parent node's ancestor generator
            yield from self.parent.get_ancestors()
            # then yield ourselves
            yield self.parent
            
    def get_closest_ancestor(self, other):
        # Finds the closest common ancestor with another node
        
        # Gets ancestor generators for both nodes
        other_ancestors = other.get_ancestors()
        my_ancestors = self.get_ancestors()
        
        # Iterate through generators, stopping when one is exhausted
        for my_ancestor, other_ancestor in itertools.zip_longest(my_ancestors, other_ancestors, fillvalue=None):
            # If my_ancestor is other_ancestor, we'll continue
            # consuming the generators until they diverge
            if my_ancestor is not other_ancestor:
                # If ancestors do not match, one of the iterators has run out.
                # or their ancestory has diverged.
                if my_ancestor is not None and other_ancestor is not None:
                    # both nodes still have ancestors, so we've diverged.
                    return my_ancestor.parent
                elif my_ancestor or other_ancestor:
                    # Only one iterator has run out.
                    # If one of the nodes we're testing matches the other's ancestor
                    # They're the closest common ancestor
                    if my_ancestor is other:
                        return other
                    elif other_ancestor is self:
                        return self
                    else:
                        # If not, the non-None ancestor's parent should be the
                        # closest common ancestor
                        if my_ancestor is not None:
                            return my_ancestor.parent
                        else:
                            return other_ancestor.parent
        
        # If we finish our loop without returning, my_ancestor and other_ancestor are the closest common ancestor
        # or the two nodes are actually the same node
        if self is other:
            return self
        else:
            return self.parent

if __name__ == "__main__":
    # set up args
    parser = argparse.ArgumentParser(description='Find the closest common ancestor for two nodes.')
    parser.add_argument('nodefile', metavar='f', 
                        type=argparse.FileType('r'), help='input file defining the nodes and relationships')
    parser.add_argument('nodes', metavar='n', nargs=2, help='Nodes to find closest ancestor for')

    args = parser.parse_args()
    
    # Set up our node tree based on the input file
    all_nodes = {}
    for line in args.nodefile:
        names = line.split()
        # If multiple nodes names on a given row
        # 2nd defines the parent node
        if len(names) > 1:
            parent = all_nodes.get(names[1], None)
            node = Node(names[0], parent)
        else:
            node = Node(names[0])
        # keep track of all the nodes by name while building our tree
        all_nodes[node.name] = node
    
    # Tree is set up now. get the nodes we're interested in
    # argparse will ensure nodes has two elements for us
    node1 = all_nodes[args.nodes[0]]
    node2 = all_nodes[args.nodes[1]]
    
    # Print their closest ancestor and return
    print(node1.get_closest_ancestor(node2))
    