import argparse
import random

if __name__ == "__main__":
    # set up args
    parser = argparse.ArgumentParser(description='Returns a randomized list of `k` ad ids from the input file')
    parser.add_argument('source', metavar='s', type=argparse.FileType('r'), 
                        help='input file defining the ad IDs, one per line')
    parser.add_argument('number', metavar='k', type=int,
                        help='Number of ads to return')

    args = parser.parse_args()
    
    # Put all the ads in a list
    id_list = []
    for line in args.source:
        id_list.append(int(line))
    # Shuffle the list. O(N) operation
    random.shuffle(id_list)
    
    # Now we just have to pop and print.
    # list.pop() is O(1), so this is O(k)
    for _ in range(args.number):
        print(id_list.pop())